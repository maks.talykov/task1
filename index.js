require("dotenv").config();
const fs = require("fs");

let result = process.env.CHAR;

if (!isNaN(Number(result))) {
  result = Number(result);
}

fs.writeFileSync(
  "./output.json",
  JSON.stringify({
    typeof: typeof result,
    result,
  })
);
